---
title: About Unity
subtitle: Why are we here, why are you here?
comments: false

---
## History

Unity-Linux was a Mandriva based Linux distribution that was started when a number of packagers left PCLinuxOS after having taken the reigns for more then a year during the lead developer and creators hiatus from the distribution. Due to multiple differences both parties thought it best to part ways, so Unity-Linux was born. Development started quickly and various packages where added. Eventually though do to lack of vision and what some would say interest Unity-Linux development slowed down crawl until it ended. The domain ended up being pointed to a personal blog, when it was rescued and the project was semi-resurrected by JMiahMan a previous developer/packager. The original focus was going to be carrying on a similar trajectory, by using Mageia, a Mandriva derivative. However, multiple events occurred that changed that direction.

1. JMiahMan the lead developer obtained a job at a local community college teaching Linux courses for students prepping for the RHCSA exams.
2. A popular and more user friendly Remix of Fedora, called Korora, halted development.

Those two things caused a rethinking and the rebirth of Unity-Linux.

## Purpose

With strong ties into Red Hat education and now a void left by Korora, Unity-Linux hopes to be a tool that can be used to educate and teach acquiring minds the Red Hat tool set, while offering a more well rounded experience then just stock Fedora.

The original purpose for Unity Linux was to create a baseline linux install to which users could create their own Linux Desktop distributions to share with family and friends for various tasks. The idea was to create scripts and various tools to allow users to learn about Linux and create something on their own. Unity was the trunk and these home brewed distributions would be the branches. Eventually it was observed not only were tools needed to help in personal distribution creation, but education as to how a Linux system works as well was really the capstone to such an endeavor. It was found from previous experience that just feeding consumers an ease of use script was not beneficial but equipping those who would like to become producers in the Red Hat ecosystem (or any Linux ecosystem for that matter) the knowledge to produce something apart from the Unity system was key, not just to maintaining Unity-Linux as a project but in feeding the overall Linux community.

## Execution

Moving further Unity-Linux has gone back to the drawing board. We are refocusing on some of the core ideas and visions that Unity-Linux was started with and peppering that with the inspiration that distributions like Korora have offered examples of. This time however instead of being based on the popular (now defunct) Mandriva desktop distribution we will be using Fedora and trying to stick close to utilizing stock Red Hat tools where possible. Work is currently on going.

## Development

Development will be synced at least weekly to [gitlab](https://gitlab.com/unity-linux/)..