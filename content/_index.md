+++

+++
Beta of Unity-Linux XFCE 30 (based off Fedora® 30) has been released. This release is for people who want to test and leave their feedback. If that is you download it from [here](https://dl.unitylinux.org/isos/beta/30/ "Beta Release") and leave your thoughts, commits, and reported bugs [here](https://discuss.unitylinux.com/d/5-xfce-beta-1-release "Beta Testing guide").