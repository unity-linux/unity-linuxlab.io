---
title: Applications for XFCE
subtitle: Your suggestions wanted
date: 2019-01-24 20:00:00 +0000
tags:
- XFCE
- ISO
- Applications

---
Daily now I have been building XFCE ISOs and doing various tweaks. At this point they are usable, but still rough around the edges. Things are starting to come together however and with that I think it's time to look at what applications we want on the XFCE ISO. We could easily just copy another great Spin like Korora, just use the defaults etc. but it is not my point to create a Fedora or Korora clone. So with that I challenge you to think a bit outside the Fedora repository. Of course you don't have to and of course give me suggestions of packages that are there, but if you can look around other distributions, github, gitlab, maybe even bitbucket and see if there's anything not packaged you'd be interested in trying or better yet know works, then please suggest it. Long story short, if it's something I need to package and offer on our repository then that would be just fine, if we can just grab it from Fedora, that's cool as well, either way let's test and get it there.

Where to post? On our [Discussion Board](https://discuss.unitylinux.com/d/3-application-list-for-xfce) of course.