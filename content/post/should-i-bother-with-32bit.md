---
title: Should I bother with 32Bit?
subtitle: it could be fun!
date: 2019-02-18 07:00:00 +0000
tags: []

---
In working with the NoCSD package (gtk3-nocsd) a user requested be installed on the XFCE Beta release it came to my attention it needed a 32Bit lib built. So for the last few days when I could muster up some time I have been working on a 32Bit Fedora docker build so I can use it to build a 32bit lib. In my opinion this route would be more easy then trying to make sure all the 32Bit libs are installed in a 64bit environment. The sad part was I couldn't find 32Bit docker build for Fedora (yes, I know why).   
  
In any case I was able to get a build done and with the exception of fixing the spec to subpackage out the lib, I think we're good to go. Now that I have a 32Bit docker build to build rpms, I am left wondering if maybe these old EeePCs I have sitting behind me could use something like Unity-Linux to run on them, and if I should mess with having 32Bit builds. So I put up a poll on [discuss](https://discuss.unitylinux.org/d/6-should-i-bother-with-a-32bit-release "Do 32Bit?"). Let me know what you think.